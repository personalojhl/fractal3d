import controlP5.*;

/*
*  \name: ejemplo-3D
*  \brief:ejemplo 3D de curso de introduccion en Semanai
*  \date: 25/09/17
*  \autor: Oscar Hinojosa
*/

/*Declaracion de variables globales de este skech*/
ControlP5 cp5;
float Angulo=0,/*angulo para rotar el 3D*/
      AnguloX=0,AnguloZ=0,
      ancho=10,
      alto=100,
      angleX=30,
      angleR1=0,
      angleR2=120,
      angleR3=240;
int Width=320, Height=300,limite=5,limiteant=limite,
    numramas=3;
Slider2D s;
Slider s1,s2;
Button B1;
void setup()
{
   cp5 = new ControlP5(this);/* creacion de constructor*/
   s2 = cp5.addSlider("recursive") /*se agrega un objeto Slider al sketch*/
   .setPosition(10,10)
   .setSize(150,10)/*tamaño del slider*//*Ubicacion del slider*/
   .setRange(0,10)/*Asignas el rango de movimiento*/
   .setValue(5)/*valor de inicio*/
   .setNumberOfTickMarks(11);   /*fatla una configuracion*/
   s = cp5.addSlider2D("Pocision")
         .setPosition(10,80)
         .setSize(80,60)
         .setArrayValue(new float[] {32, 30})
         //.disableCrosshair()
         ;
  s1= cp5.addSlider("Ramas")
   .setPosition(10,40)
   .setSize(150,10)/*tamaño del slider*//*Ubicacion del slider*/
   .setRange(1,5)/*Asignas el rango de movimiento*/
   .setValue(3)/*valor de inicio*/
   .setNumberOfTickMarks(5);   /*fatla una configuracion*/

 /*renderer*/
 size(800,600,P3D);
 cp5.addLabel("Insrucciones presione boton Teclado")
   .setPosition(10,540)
   ;
  B1 =cp5.addButton("Teclado")
     .setValue(0)
     .setPosition(10,550)
     .setSize(200,20)
     ;
}

void draw()
{
  
  Width= int(s.getArrayValue()[0])*10;
  Height=int(s.getArrayValue()[1])*10;
  
 background(0);
 pushMatrix();
//   translate(((Width/1.5)-(Width/10)),((Height/1.5)-(Height/10)),0);
     translate(Width, Height, 0);
     rotateZ( radians( AnguloZ ) );
   rotateY( radians( Angulo ) );
   rotateX(radians(AnguloX));
     noStroke();
    Arbol(float(1)); 
     
 popMatrix();
}

void Arbol (float level)
{
  if( level >= limite )
  {
   return; 
  }
  float anchoN = ancho / level,
        altoN  = alto / level,
        altoH  = alto / (level + 1 ),
        desp   = - (altoN - altoH)/2;/*Desplazamiento del hijo sigueinte*/
  
  box(anchoN,altoN,anchoN);/*sustituye al rectangulo por ser 3D*/
  int i=0;
  do
  {
   int angulo=360/numramas;
       
    pushMatrix();/*rotacion en X para tener una ramita en un angulo*/
    translate(0,-altoH,0);
    rotateY(radians(angulo*i)); 
    rotateX(radians(angleX));
    translate(0 , desp*1.1 ,0);
    Arbol(level+1);
    popMatrix();
    
  
  }while(i++ <= numramas);
}  
void keyPressed ( KeyEvent k) 
{
  int keypress= k.getKeyCode();
  println(keypress);/*imprimir la letra presionada*/
  if ( 65 == keypress) /*65 = a*/
  {
    Angulo -= 10;
  }
  if ( 68 == keypress) /*68=d*/
  {
    Angulo += 10;
  }

    if ( 81 == keypress) /*81=q*/
  {
    AnguloZ += 10;
  }
  
  
    if ( 83 == keypress) /*83=s*/
  {
    AnguloX -= 10;
  }
  
  
  if ( 87 == keypress) /*87=w*/
  {
    AnguloX += 10;
  }
  
  
    if ( 69 == keypress) /*69=e*/
  {
    AnguloZ -= 10;
  }
  
  
  
  if (82 == keypress)    /*82=r*/
  {
     Angulo=0;
     AnguloZ=0;
     AnguloX=0;
  }
  
  
  
  
   if ( 37 == keypress) /*37 izquierdas*/
  {
    if( 0 < limite)
    {
    limite -= 1 ;
    s2.setValue(limite);
    }
    else
    {
      println("no te pases");
    }
  }
  
  
  
  if ( 39 == keypress) /*39 derecha*/
  {
    if(10 > limite)
    {
    limite += 1 ;
    s2.setValue(limite);
     }
    else
    {
     println("no te pases"); 
    }
  }
  
  if ( 40 == keypress) /*40 arriba*/
  {
    if( 0 < numramas)
    {
    numramas -= 1 ;
    s1.setValue(numramas);
    println(numramas);
    }
    else
    {
      println("no te pases");
    }
  }
  
  
  if ( 38 == keypress) /*38 arriba*/
  {
    if(5 > numramas)
    {
    numramas += 1 ;
    s1.setValue(numramas);
    println(numramas);
     }
    else
    {
     println("no te pases"); 
    }
  }
  
  
  
  
}
void recursive(float v)
{
  println("valor: " + v);
  limite=int(v);
}
void Ramas(float r)
{
 println("ramas: " + r);
 numramas=int(r);
}
public void Teclado(int valor)
{
  println("boton teclado presionado:"+valor);
  javax.swing.JOptionPane.showMessageDialog(null, 
  "Intrucciones: \n" +
  "Flechas arriba para aumentar el numero de ramas\n"+
  "Flecha abajo disminuir el numero de ramas.\n"+
  "Flecha derecha aumentar el numero de recursividad\n"+
  "Flechas izquierda dsiminuir el numero de recursividad\n"+
  "Teclas 'q' y 'e' para mover el eje Z\n"+
  "Teclas 'w' y 's' para el eje Y\n"+
  "Teclas 'a' y 'd' para el eje X\n"
  );
}